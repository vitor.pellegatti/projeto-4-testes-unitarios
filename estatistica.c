#include "estatistica.h"
#include <math.h>

//funcao que vai calcular a media aritmetica dos n valores fornecidos no vetor v
double media(int n, double *v){
    
    if(n != 0){
        double result = 0.0;
        for(int i = 0; i < n; i ++){
            result += v[i];
        }

        return result/n;
    }

    return 0;
}

//funcao que vai calcular o desvio padrao dos n valores fornecidos no vetor v.
//A funcao sempre retorna um numero com precisao de 3 casas decimais
double desvio(int n, double *v){
    if(n != 0){//precisamos garantir que o vetor nao esteja vazio
        // calcular a media dos elementos do vetor
        double valor_media = media(n, v);
        
        //fazendo a somatoria
        double somatoria = 0.0;
        for(int i = 0; i < n; i ++){
            somatoria += pow((v[i] - valor_media),2);
        }

        //pequena trapaca para conseguir arredondar o numero
        int temp = sqrt(somatoria/(n-1)) * 1000; 
        double res = temp / 1000.0;

        //retornando o desvio padrao
        return res;
    }
    return 0;
}