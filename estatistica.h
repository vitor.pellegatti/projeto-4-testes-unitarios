#ifndef ESTATISTICA_H_
#define ESTATISTICA_H_

//chamada da funcao de media
double media(int n, double *v);

//chamada da funcao de desvio padrao
double desvio(int n, double *v);

#endif