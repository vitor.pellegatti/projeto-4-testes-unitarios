#include "unity.h"
#include "estatistica.h"

void setUp() {
}

void tearDown() {
}

void test_media_zero(){
    double v[0];
    double res = media(0,v);
    TEST_ASSERT_EQUAL_DOUBLE(0,res);
}

void test_media_positivos(){
    double v[5] = {1,2,3,4,5};
    double res = media(5,v);
    TEST_ASSERT_EQUAL_DOUBLE(3,res);
}

void test_media_numeros_quebrados(){
    double v[5] = {2.3,2.1,2.6,2.2,2};
    double res = media(5,v);
    TEST_ASSERT_EQUAL_DOUBLE(2.24,res);
}

void test_media_grandes(){
    double v[5] = {1500,1200,1000,800,500};
    double res = media(5,v);
    TEST_ASSERT_EQUAL_DOUBLE(1000,res);
}

void test_media_negativos(){
    double v[5] = {-10,-5,5,10,20};
    double res = media(5,v);
    TEST_ASSERT_EQUAL_DOUBLE(4,res);
}

void test_desvio_positivos(){
    double v[6] = {1,2,3,4,5,6};
    double res = desvio(6,v);
    TEST_ASSERT_EQUAL_DOUBLE(1.870,res);
}

void test_desvio_com_negativo(){
    double v[4] = {-4,5,6,9};
    double res = desvio(4,v);
    TEST_ASSERT_EQUAL_DOUBLE(5.597,res);
}

void test_desvio_com_virgulas(){
    double v[4] = {0.42,0.52,0.58,0.62};
    double res = desvio(4,v);
    TEST_ASSERT_EQUAL_DOUBLE(0.086,res);
}

void test_desvio_varios_elementos(){
    double v[13] = {90,94,53,68,79,84,87,72,70,69,65,89,85};
    double res = desvio(13,v);
    TEST_ASSERT_EQUAL_DOUBLE(12.099,res);
}

void test_desvio_zero(){
    double v[0];
    double res =  desvio(0,v);
    TEST_ASSERT_EQUAL_DOUBLE(0.0,res);
}

int main(){
    UNITY_BEGIN();
    RUN_TEST(test_media_positivos);
    RUN_TEST(test_media_grandes);
    RUN_TEST(test_media_negativos);
    RUN_TEST(test_media_numeros_quebrados);
    RUN_TEST(test_media_zero);
    RUN_TEST(test_desvio_positivos);
    RUN_TEST(test_desvio_com_negativo);
    RUN_TEST(test_desvio_com_virgulas);
    RUN_TEST(test_desvio_varios_elementos);
    RUN_TEST(test_desvio_zero);
    return UNITY_END();
}